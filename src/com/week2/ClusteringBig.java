package com.week2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*In this, the input provided contains the vertices 
 * in the form of binary representation with 24 bits each. 
 * The hamming distance between the vertices is the edge cost.
 */
public class ClusteringBig {

	private Map<String, Integer> vertices;
	private int numOfClustersK;
	private int[] parentNodeIndex;
	private int[] size;
	
	private Map<Integer, ArrayList<Edge>> edgesForDistance;
	private class Edge{
		private String node1;
		private String node2;
		
        Edge(String node1,String node2){
			this.node1 = node1;
			this.node2 = node2;
		}
	}
	
	public ClusteringBig(FileReader file) throws IOException{
		
		BufferedReader reader = new BufferedReader(file);
		String line = reader.readLine();
		String[] firstLine = line.trim().split(" ");
		int numOfNodes = Integer.parseInt(firstLine[0]);
		int numOfBits = Integer.parseInt(firstLine[1]);
		
		parentNodeIndex = new int[numOfNodes+1];
		size = new int[numOfNodes+1];
		vertices = new HashMap<String, Integer>(); // the value is the index corresponding to the bits sequence which is used in union operation
		edgesForDistance = new HashMap<Integer, ArrayList<Edge>>();
		edgesForDistance.put(1, new ArrayList<Edge>());
		edgesForDistance.put(2, new ArrayList<Edge>());
		
		for(int i = 0; i < numOfNodes+1; i++){
			parentNodeIndex[i] = i; // initially all nodes will be pointing to themselves
			size[i] = 1; 
		}
		int index = 1;
		while((line = reader.readLine()) != null){
			line = line.trim();
			line = line.replaceAll(" ", "");
			if(!vertices.containsKey(line)){
			vertices.put(line, index); // this makes sure there are no duplicates
			index++;
			}
		}
		numOfClustersK = vertices.size();
		// method to compute edges with spacing of 1 and 2
		oneDistTwoDistEdgesComputation(numOfBits);
	}
	
	/**
	 * Returns all possible list of bitstrings with "bitsSet" being variables to change.
	 * @param bitStringSize - how many bits to flip.
	 * @param inputBitString 
	 * @return
	 */
	private Set<String> getAllPossibleStrings(int bitsToFlip, String inputVertexBitString) { //bitsSet is usually 1 or 2.
		int bitStringSize = inputVertexBitString.length();
		Set<String> bitStrings = new HashSet<String>();
		if (bitsToFlip ==1) {
			for (int j =0; j<bitStringSize; j++) {
				char [] arr = inputVertexBitString.toCharArray();
				arr[j]= arr[j]=='0'? '1':'0';
				bitStrings.add(new String(arr));
			}			
		}
		if(bitsToFlip==2) {
			for(int i =0 ; i<bitStringSize; i++) {
				for (int j =0; j<bitStringSize; j++) {
					if (i == j) {
						continue;
					}
					char [] arr = inputVertexBitString.toCharArray();
					arr[j]= (arr[j]=='0')? '1':'0';
					arr[i]= (arr[i]=='0')? '1':'0';
					bitStrings.add(new String(arr));
				}
			}
		}
		return bitStrings;
	}

	private void oneDistTwoDistEdgesComputation(int numOfBits){
		for(String vertex: vertices.keySet()){ //For every bitString.

			for (String oneBitToggled: getAllPossibleStrings(1,vertex)) {
				if(vertices.containsKey(oneBitToggled)){
					edgesForDistance.get(1).add(new Edge(vertex, oneBitToggled));
				}
			}

			for (String oneBitToggled: getAllPossibleStrings(2,vertex)) {
				if(vertices.containsKey(oneBitToggled)){
					edgesForDistance.get(2).add(new Edge(vertex, oneBitToggled));
				}
			}
		}
	}

    private int findRootIndex(String node) {
		
    	int nodeIndex = vertices.get(node);
		while(nodeIndex!= parentNodeIndex[nodeIndex]){
			nodeIndex = parentNodeIndex[nodeIndex];
		}
		return nodeIndex;
	}
    
    public void union(String p, String q){
		
		int rootPIndex = findRootIndex(p);
		int rootQIndex = findRootIndex(q);
		
		if(rootPIndex == rootQIndex){
			return;
		}
		
		if(size[vertices.get(p)] > size[vertices.get(q)]){
			parentNodeIndex[rootQIndex] = rootPIndex;
			size[rootPIndex] += size[rootQIndex];

		}else{
			parentNodeIndex[rootPIndex] = rootQIndex;
			size[rootQIndex] += size[rootPIndex] ;
		}
		numOfClustersK--;
    }
    
    public int numOfClustersk_computation(int minSpacingToFindK){
    	int spacing = 1;
    	while(spacing < minSpacingToFindK){
    		ArrayList<Edge> toBeClusteredEdges = edgesForDistance.get(spacing);
    		for(Edge edgeUnderConsideration: toBeClusteredEdges){
    			union(edgeUnderConsideration.node1, edgeUnderConsideration.node2);
    		}
    		spacing++;
    	}
    	return numOfClustersK;
    }
    
	public static void main(String[] args) throws IOException {
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms 3/Week2/clusteringBig.txt");//145245
		ClusteringBig clustering = new ClusteringBig(file);
		System.out.println("Number of clusters:"+ clustering.numOfClustersk_computation(3));
	}
}

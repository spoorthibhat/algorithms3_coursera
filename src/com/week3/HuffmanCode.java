package com.week3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class HuffmanCode {

	private PriorityQueue<Node> minWeightNodeHeap;
	private int numOfSymbols;
	private Node root;
	private List<Integer> lengthOfSymbols;

	private class Node{
		private int weight;
		private Node parent;
		private Node leftChild;
		private Node rightChild;
		public int depth;

		public Node(int weight, Node parent, Node leftChild, Node rightChild){
			this.weight = weight;
			this.parent = parent;
			this.leftChild = leftChild;
			this.rightChild = rightChild;
			depth = 0;
		}
	}

	public HuffmanCode(FileReader file) throws IOException{

		BufferedReader reader = new BufferedReader(file);
		String line = reader.readLine();
		root = null;
		numOfSymbols = Integer.parseInt(line.trim());
		minWeightNodeHeap = new PriorityQueue<Node>(1,new weightComparator());
		lengthOfSymbols = new ArrayList<Integer>();

		// store all the leaf nodes with their weights in the heap!!
		while((line = reader.readLine()) != null){
			int weightToBeAdded = Integer.parseInt(line.trim());
			Node toBeAdded = new Node(weightToBeAdded, null, null, null);
			minWeightNodeHeap.add(toBeAdded);
		}
	}

	public void minMaxLength(){

		Integer[] length = new Integer[numOfSymbols];
		buildTree();
		depthComputation(root, 0);

		length = lengthOfSymbols.toArray(length);
		Arrays.sort(length);
		System.out.println("MinLength: " +length[0]);
		System.out.println("MaxLength: " +length[length.length - 1]);
	}

	private void buildTree(){
		while(minWeightNodeHeap.size() >= 2){
			Node node1 = minWeightNodeHeap.poll();
			Node node2 = minWeightNodeHeap.poll();
			merge(node1, node2);
		}
		root = minWeightNodeHeap.peek(); // finally the last parent node that was added to the queue is the root.
	}

	private void merge(Node nodeFirst, Node nodeSecond){
		Node left = (nodeFirst.weight > nodeSecond.weight)? nodeSecond: nodeFirst; // just the way I want that helps my debugging
		Node right = (nodeFirst.weight < nodeSecond.weight)? nodeSecond: nodeFirst;
		Node parentNode = new Node(nodeFirst.weight + nodeSecond.weight, null, left, right);
		nodeFirst.parent = parentNode;
		nodeSecond.parent = parentNode;
		minWeightNodeHeap.add(parentNode);
	}
	
	private void depthComputation(Node current, int depth){
		// depth of the leaf nodes is the length of the code corresponding to it when encoded.

		if (current != null){
			current.depth = depth;
			if(current.leftChild == null && current.rightChild == null){
				lengthOfSymbols.add(current.depth);
			}
			depthComputation(current.leftChild,depth+1);
			depthComputation(current.rightChild, depth +1);
		}
	}

	private class weightComparator implements Comparator<Node>{

		public int compare(Node one, Node two){
			if(one.weight < two.weight){
				return -1;
			}else if(one.weight > two.weight){
				return +1;
			}else{
				return 0;
			}
		}
	}

	public static void main(String[] args) throws IOException{
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms 3/Week3/huffman.txt");
		HuffmanCode code = new HuffmanCode(file);
		code.minMaxLength();

	}
}
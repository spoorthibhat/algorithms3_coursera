package com.week2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Clustering {

	private int numOfClustersK;
	private Edge[] edges;
	private int[] parentNode;
	private int[] size;
	
	private class Edge{
		private int node1;
		private int node2;
		private int cost;
		
		Edge(int node1,int node2,int cost){
			
			this.node1 = node1;
			this.node2 = node2;
			this.cost = cost;
		}
	}
	
	public Clustering(FileReader file) throws NumberFormatException, IOException{
		
		BufferedReader reader = new BufferedReader(file);
		String line = reader.readLine();
		int numOFNodes = Integer.parseInt(line);
		numOfClustersK = numOFNodes;
		parentNode = new int[numOFNodes+1];
		size = new int[numOFNodes+1];
		List<Edge> edgeList = new ArrayList<Edge>();
		
		while((line = reader.readLine()) != null){
			String[] rowElement = line.trim().split(" ");
			Edge inputEdge = new Edge(Integer.parseInt(rowElement[0]), Integer.parseInt(rowElement[1]), Integer.parseInt(rowElement[2]));
			edgeList.add(inputEdge);
		}
		edges = new Edge[edgeList.size()];
		edges = edgeList.toArray(edges);
		Arrays.sort(edges, costComparator()); // heap was not used as retrieving the minimum would call heapify everytime that would increase complexity
		
		for(int i = 0; i < numOFNodes+1; i++){
			parentNode[i] = i; // initially all nodes will be pointing to themselves
			size[i] = 1; 
		}
	}
	
	public int find(int node) {
		
		while(node!= parentNode[node]){
			node = parentNode[node];
		}
		return node;
	}
	
	public void union(int p, int q){
		
		int rootP = find(p);
		int rootQ = find(q);
		
		if(rootP == rootQ){
			return;
		}
		
		if(size[p] > size[q]){
			parentNode[rootQ] = rootP;
			size[rootP] += size[rootQ];
		}else{
			parentNode[rootP] = rootQ;
			size[rootQ] += size[rootP] ;
		}
		
		numOfClustersK--;
	}
	
	public int computingTheSpacing(int k){
		int i = 0;
		while( numOfClustersK != k){
			Edge toBeClustered = edges[i];
			union(toBeClustered.node1, toBeClustered.node2); // number of clusters will be decremented in union method
			i++;
		}

		while(find(edges[i].node1) == find(edges[i].node2)){
			i++;
		}
		return edges[i].cost;
	}
	
	public Comparator<Edge> costComparator(){
		return new edgeCostAscending();
	}
	
	private class edgeCostAscending implements Comparator<Edge>{
		public int compare(Edge edge1, Edge edge2){
			if(edge1.cost > edge2.cost) {
				return +1;
			}else if (edge1.cost < edge2.cost) {
				return -1;
			} else {
				return 0;
			}
		}
	}

	public static void main(String[] args) throws IOException {
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms 3/Week2/clustering1.txt"); //106
		Clustering clustering = new Clustering(file);
		System.out.println(clustering.computingTheSpacing(4));
		
	}

}

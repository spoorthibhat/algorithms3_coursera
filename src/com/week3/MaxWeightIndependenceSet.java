package com.week3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MaxWeightIndependenceSet {

	private int[] vertices;
	private int[] maxWeights;
	private int numOfVertices;
	private Map<Integer, Integer> independenceSet; // <vertex,1>
	
	public MaxWeightIndependenceSet(FileReader file) throws IOException{
		BufferedReader reader = new BufferedReader(file);
		String line = reader.readLine().trim();
		numOfVertices = Integer.parseInt(line);
		vertices = new int[numOfVertices+1];
		maxWeights = new int[numOfVertices+1];
		independenceSet = new HashMap<Integer, Integer>();
		
		vertices[0] = 0;
		int i = 1;
		while((line = reader.readLine()) != null){
			vertices[i] = Integer.parseInt(line.trim());
			maxWeights[i] = 0; // initially
			i++;
		}
	}

	public void computeIndependenceSet(){
		maxWeightComputation();
		//System.out.println(maxWeights[numOfVertices]);
		int i = numOfVertices;
		while(i >= 1){
			int mWMinus1 = maxWeights[i-1];
			int mwMinus2 = (i == 1)? 0 : maxWeights[i-2];
			if(mWMinus1 > (mwMinus2 + vertices[i])){
				i = i - 1;
			}else{
				independenceSet.put(i, 1);
				i = i - 2;
			}
		}
		printBinaryOutput();
	}

	private void maxWeightComputation(){
		maxWeights[1] = vertices[1];
		
		for(int i = 2; i <= numOfVertices; i++){
			maxWeights[i] = Math.max(maxWeights[i-1], maxWeights[i-2] + vertices[i]);
		}
	}

	private void printBinaryOutput(){
		int statusOfVertices[] = {1,2,3,4,17,117,517,997};
		for(int i = 0; i < statusOfVertices.length; i++)
		{		
			if (independenceSet.containsKey(statusOfVertices[i])){
				System.out.print(independenceSet.get(statusOfVertices[i]));
			} else {
				System.out.print("0");
			}
		}
	}

	public static void main(String[] args) throws IOException {
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms 3/Week3/MWIS.txt");
		MaxWeightIndependenceSet mwIS  = new MaxWeightIndependenceSet(file);
		mwIS.computeIndependenceSet();
	}
}

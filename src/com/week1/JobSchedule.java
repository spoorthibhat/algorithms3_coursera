package com.week1;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

public class JobSchedule {

	/**
	 * @param args
	 */
	private Job[] jobs;
	private int totalJobsCount;
	
	private class Job{
		
		private int weight;
		private int length;
		private int difference;
		private double ratio;
		
		Job(int weight, int length){
			this.weight = weight;
			this.length = length;
			difference = weight - length;
			ratio = weight * 1.0 / length;
		}
	}
	
	public JobSchedule(FileReader file) throws NumberFormatException, IOException{
		
		BufferedReader reader = new BufferedReader(file);
		totalJobsCount = Integer.parseInt(reader.readLine());
		jobs = new Job[totalJobsCount];
		String line;
		int i = 0;
		while((line = reader.readLine()) != null){
			String[] jobelements = line.split(" ");
			jobs[i] = new Job(Integer.parseInt(jobelements[0]), Integer.parseInt(jobelements[1]));
			i++;
			if(i == totalJobsCount){
				break;
			}
		}
		reader.close();
	
	}
	
	public Comparator<Job> priorityOnDifference(){
		return new DiffernceComparator();
	}
	
	public Comparator<Job> priorityOnRatio(){
		return new RatioComparator();
	}
	
	private class DiffernceComparator implements Comparator<Job>{
		
		public int compare(Job first, Job second){
			
			if(first.difference == second.difference) {
				
				if(first.weight > second.weight){ // breaking ties by weight
					return +1;
				}else if (first.weight < second.weight){
					return -1;
				}else {
					return 0;
				}
			}else if(first.difference > second.difference) {
				return +1;
			}else{
				return -1;
			}
		}
	}
	
	private class RatioComparator implements Comparator<Job>{
		public int compare(Job first, Job second) {
			
			if(Double.compare(first.ratio, second.ratio)== 0) {
				if(first.weight > second.weight) {
					return +1;
				}else if(first.weight < second.weight){
					return -1;
				}else{
					return 0;
				}
			} else if(Double.compare(first.ratio, second.ratio) > 0){
				return +1;
			} else {
				return -1;
			}
		}
	}
	public long weightedSumForDifference(){
		
		Arrays.sort(jobs,priorityOnDifference());
		return weightedSum();
	}
	
	public long weightedSumForRatio(){
		Arrays.sort(jobs,priorityOnRatio());
		return weightedSum();
	}
	private long weightedSum(){
		
		long sum = 0;
		long previousCompletionTime = 0;
		for(int i = totalJobsCount-1; i >= 0 ; i-- ){
			long completionTime = jobs[i].length + previousCompletionTime;
			sum += jobs[i].weight * completionTime;
			if(sum < 0){
				System.out.println(i);
				System.out.println(completionTime);
				break;
			}
			previousCompletionTime = completionTime;
		}
		
		return sum;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms 3/Week1/Jobs.txt");
		JobSchedule schedulingObject = new JobSchedule(file);
		
		
		System.out.println("WeightedSum when jobs are scheduled depending on difference : " + schedulingObject.weightedSumForDifference());
		System.out.println("WeightedSum when jobs are scheduled depending on ratio : " + schedulingObject.weightedSumForRatio());
		
		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);
		}
		//1175612
		//1142691
		

	}

}

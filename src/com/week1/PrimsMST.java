package com.week1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class PrimsMST {

	/**
	 * @param args
	 */
	private ArrayList<Edge>[] edgeList;
	private boolean[] isNodeVisited;
	private int numberOfNodes;
	private ArrayList<Integer> mstVertices;
	private int overallCost;
	private int firstNode;
	boolean firstNodeNotRecorded = true;
	
	
	private class Edge{
		
		private int vertex1;
		private int vertex2;
		private int cost;
		
		public Edge(int vertex1,int vertex2,int cost){
			
			this.vertex1 = vertex1;
			this.vertex2 = vertex2;
			this.cost = cost;
			
		}
	}
	
	public PrimsMST(FileReader file) throws IOException{
		
		BufferedReader reader = new BufferedReader(file);
		String line = reader.readLine();
		String[] nodesEdgesNumbers = line.split(" ");
		numberOfNodes = Integer.parseInt(nodesEdgesNumbers[0]);
		overallCost = 0;
		edgeList = new ArrayList[numberOfNodes + 1];
		mstVertices = new ArrayList<Integer>();
		isNodeVisited = new boolean[numberOfNodes + 1];
		for(int i = 0; i <= numberOfNodes; i++){
			isNodeVisited[i] = false;
			edgeList[i] = new ArrayList<Edge>();
		}
		
		while((line = reader.readLine()) != null){
			String[] lineElements = line.split(" ");
			int node1 = Integer.parseInt(lineElements[0]);
			int node2 = Integer.parseInt(lineElements[1]);
			int edgeCost = Integer.parseInt(lineElements[2]);
			Edge edge = new Edge(node1, node2, edgeCost);
			edgeList[node1].add(edge);
			edgeList[node2].add(edge);
			
			if (firstNodeNotRecorded) {
				firstNode = node1;
				firstNodeNotRecorded = false;
			}
		}
	}
	
	public int mst(){
		// adding the first node
		mstVertices.add(firstNode); 
		int recentlyAdded = firstNode;
		isNodeVisited[recentlyAdded] = true;

		PriorityQueue<Edge> minCostEdgeQueue = new PriorityQueue<Edge>(1, minCostPriority());

		while(mstVertices.size() != numberOfNodes) {
			minCostEdgeQueue = addValidEdges(recentlyAdded, minCostEdgeQueue);
			recentlyAdded = getNextNode(minCostEdgeQueue);
		}
		return overallCost;
	}
	
	private PriorityQueue<Edge> addValidEdges(int recentlyAddedVertex, PriorityQueue<Edge> minCostQ){
		for(Edge edge: edgeList[recentlyAddedVertex]){
			if(!isNodeVisited[edge.vertex1] || !isNodeVisited[edge.vertex2]){
				minCostQ.add(edge);
			}
		}
		return minCostQ;
	}
	
	private int getNextNode(PriorityQueue<Edge> minCostEdgeQueue){
		
		int nextNodeIntoMST;
		Edge rootEdge = minCostEdgeQueue.peek();
		while(isNodeVisited[rootEdge.vertex1] && isNodeVisited[rootEdge.vertex2]){
			minCostEdgeQueue.poll();
			rootEdge = minCostEdgeQueue.peek();
		}

		rootEdge = minCostEdgeQueue.poll();
		overallCost += rootEdge.cost;
		if(!isNodeVisited[rootEdge.vertex1]){
			nextNodeIntoMST = rootEdge.vertex1;
		}else{ //This is not a concern. But a good code might have hadded an else condigtion more like if condition but for vertex2.
			nextNodeIntoMST = rootEdge.vertex2;
		}
		mstVertices.add(nextNodeIntoMST);
		isNodeVisited[nextNodeIntoMST] = true;
		
		//System.out.println(nextNodeIntoMST);
		return nextNodeIntoMST;
		
	}
	public Comparator<Edge> minCostPriority(){
		return new CostComparator();
	}
	
	private class CostComparator implements Comparator<Edge>{
		
		public int compare(Edge one, Edge two){
			
			if(one.cost< two.cost){
				return -1;
			}else if(one.cost > two.cost){
				return +1;
			}else{
				return 0;
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms 3/Week1/edges.txt");
		PrimsMST primsMst = new PrimsMST(file);
		System.out.println(primsMst.mst());
		
		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);
		}
		

	}

}

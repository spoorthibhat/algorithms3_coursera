package com.week4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class KnapsackProblem {

	private int[][] subproblemValues;
	private int[] itemValue;
	private int[] itemWeight;
	private int knapsackSize;
	private int numberOfItems;
	
	public KnapsackProblem(FileReader file) throws IOException{
		
		BufferedReader reader = new BufferedReader(file);
		String line;
		line = reader.readLine();
		String[] row1 = line.trim().split(" ");
		knapsackSize = Integer.parseInt(row1[0]);
		numberOfItems = Integer.parseInt(row1[1]);
		itemValue = new int[numberOfItems + 1];
		itemWeight = new int[numberOfItems + 1];
		subproblemValues = new int[knapsackSize + 1][2];
		
		for(int i = 0; i <= knapsackSize; i++){
			subproblemValues[i][0] = 0;
		}
		int index = 1;
		while((line = reader.readLine()) != null){
			
			String[] eachRow = line.trim().split(" ");
			itemValue[index] = Integer.parseInt(eachRow[0]);
			itemWeight[index] = Integer.parseInt(eachRow[1]);
			index++;
		}
	}
	
	public void optimalSolution(){
		
		for(int i = 1; i <= numberOfItems; i++){
			for(int x = 0; x<= knapsackSize; x++){
				if(i%2 == 0){
					if(x < itemWeight[i]){
						subproblemValues[x][0] = subproblemValues[x][1];
					}else{
					subproblemValues[x][0] = Math.max(subproblemValues[x][1], itemValue[i] + subproblemValues[x - itemWeight[i]][1]);
					}
				}else{
					if(x < itemWeight[i]){
						subproblemValues[x][1] = subproblemValues[x][0];
					}else{
					subproblemValues[x][1] = Math.max(subproblemValues[x][0], itemValue[i] + subproblemValues[x - itemWeight[i]][0]);
					}
				}
			}
		}
		if(numberOfItems % 2 == 0){
			System.out.println(subproblemValues[knapsackSize][0]);
		}else{
			System.out.println(subproblemValues[knapsackSize][1]);
		}
	}
	public static void main(String[] args) throws IOException {
		
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms 3/Week4/knapsack_big.txt");
		// 2493893 for knapsack1.txt
		// 4243395 for knapsack_big.txt
		KnapsackNew kp = new KnapsackNew(file);
		kp.optimalSolution();
	}

}
